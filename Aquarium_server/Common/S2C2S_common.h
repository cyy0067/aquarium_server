﻿#pragma once

namespace S2C2S {


	//Message ID that replies to each RMI method. 
               
    static const ::Proud::RmiID Rmi_RequestLogin = (::Proud::RmiID)(1000+1);
               
    static const ::Proud::RmiID Rmi_NotifyLoginSuccess = (::Proud::RmiID)(1000+2);
               
    static const ::Proud::RmiID Rmi_NotifyLoginFailed = (::Proud::RmiID)(1000+3);
               
    static const ::Proud::RmiID Rmi_JoinGameRoom = (::Proud::RmiID)(1000+4);
               
    static const ::Proud::RmiID Rmi_JoinInGame = (::Proud::RmiID)(1000+5);
               
    static const ::Proud::RmiID Rmi_LeaveGameRoom = (::Proud::RmiID)(1000+6);
               
    static const ::Proud::RmiID Rmi_Room_Appear = (::Proud::RmiID)(1000+7);
               
    static const ::Proud::RmiID Rmi_Room_Disappear = (::Proud::RmiID)(1000+8);
               
    static const ::Proud::RmiID Rmi_Game_Appear = (::Proud::RmiID)(1000+9);
               
    static const ::Proud::RmiID Rmi_CountStart = (::Proud::RmiID)(1000+10);
               
    static const ::Proud::RmiID Rmi_GameStart = (::Proud::RmiID)(1000+11);
               
    static const ::Proud::RmiID Rmi_Player_SetReady = (::Proud::RmiID)(1000+12);
               
    static const ::Proud::RmiID Rmi_Player_SetResponse = (::Proud::RmiID)(1000+13);
               
    static const ::Proud::RmiID Rmi_PlayerInfo = (::Proud::RmiID)(1000+14);
               
    static const ::Proud::RmiID Rmi_Player_Move = (::Proud::RmiID)(1000+15);
               
    static const ::Proud::RmiID Rmi_Player_Chat = (::Proud::RmiID)(1000+16);
               
    static const ::Proud::RmiID Rmi_Player_Shoot = (::Proud::RmiID)(1000+17);
               
    static const ::Proud::RmiID Rmi_Player_GetHP = (::Proud::RmiID)(1000+18);
               
    static const ::Proud::RmiID Rmi_Player_SetHP = (::Proud::RmiID)(1000+19);
               
    static const ::Proud::RmiID Rmi_Show_Player_Color = (::Proud::RmiID)(1000+20);
               
    static const ::Proud::RmiID Rmi_Room_weather = (::Proud::RmiID)(1000+21);
               
    static const ::Proud::RmiID Rmi_Room_Item = (::Proud::RmiID)(1000+22);
               
    static const ::Proud::RmiID Rmi_Player_Kill = (::Proud::RmiID)(1000+23);
               
    static const ::Proud::RmiID Rmi_Get_selete = (::Proud::RmiID)(1000+24);
               
    static const ::Proud::RmiID Rmi_Set_selete = (::Proud::RmiID)(1000+25);
               
    static const ::Proud::RmiID Rmi_Get_END = (::Proud::RmiID)(1000+26);
               
    static const ::Proud::RmiID Rmi_Set_END = (::Proud::RmiID)(1000+27);

	// List that has RMI ID.
	extern ::Proud::RmiID g_RmiIDList[];
	// RmiID List Count
	extern int g_RmiIDListCount;

}


 
