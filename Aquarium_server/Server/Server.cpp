﻿// Server.cpp : 애플리케이션에 대한 진입점을 정의합니다.
//

#include "framework.h"
#include "Server.h"
#include "AdoWrap.h"
#include "../Common/S2C2S_proxy.cpp"
#include "../Common/S2C2S_stub.cpp"
#include "../Common/S2C2S_common.cpp"

int main()
{
	Aquarium_server server; // 서버 객체
	server.Run(); // 서버 런타임 시작

	return 0;
}

void Aquarium_server::Run() // 런타임 함수 정의
{
	// 객체 시작
    m_server = shared_ptr<CNetServer>(CNetServer::Create());

    // 서버에서 클라 연결을 받으면 호출
    m_server->OnClientJoin = [](CNetClientInfo* info)
    {
        cout << "Client " << info->m_HostID << " joined.\n";
    };

    // 클라가 나가면 호출
    m_server->OnClientLeave = [this](CNetClientInfo* info, ErrorInfo*, const ByteArray&)
    {
        cout << "Client " << info->m_HostID << " went out.\n";

        CriticalSectionLock lock(m_critSec, true);

        // 플레이어가 P2P 방에 있었을 경우
        if (m_Client_Infos[info->m_HostID]->m_groupID != 0)
        {
            // P2P 방 아이디
            HostID GroupID = static_cast<HostID>(m_Client_Infos[info->m_HostID]->m_groupID);

            // 클라이언트의 그룹아이디 값 초기화
            m_Client_Infos[info->m_HostID]->m_groupID = 0;

            // P2P 방에서 클라이언트 삭제
            m_server->LeaveP2PGroup(info->m_HostID, GroupID);

            // 만약 방에 나만 있었다면, P2P 방을 삭제.
            if (m_Group_Infos[GroupID]->m_player_num == 1)
            {
                // P2P 방 정보 및 데이터 삭제
                m_Group_Infos.erase(GroupID);
                m_playerGroups.erase(GroupID);

                m_server->DestroyP2PGroup(GroupID);
            }
            // 방에 나 이외에 다른 사람도 있었다면 P2P 방을 계속 보관.
            else if (m_Group_Infos[GroupID]->m_player_num > 1)
            {
                // 플레이어 감소
                m_Group_Infos[GroupID]->m_player_num--;

                if (m_Client_Infos[info->m_HostID]->m_Team.compare(L"Sapphire") == 0)
                {
                    m_Group_Infos[GroupID]->m_S_Team_num--;
                }
                if (m_Client_Infos[info->m_HostID]->m_Team.compare(L"Ruby") == 0)
                {
                    m_Group_Infos[GroupID]->m_R_Team_num--;
                }

                // 멤버 삭제
                m_playerGroups[GroupID].RemoveOneByValue(info->m_HostID);

                // P2P 방에 있는 클라이언트에게 게임 방 퇴장 알림.
                for (auto member : m_playerGroups.find(GroupID)->second)
                {
                    m_proxy.Room_Disappear(member, RmiContext::ReliableSend, m_Client_Infos[info->m_HostID]->m_TeamNum);
                }

                // 입장한 정원이 모두 준비가 됐다면
                if (m_Group_Infos[GroupID]->m_In_game_num == m_Group_Infos[GroupID]->m_player_num)
                {
                    // 타이머를 설정하고
                    m_Group_Infos[GroupID]->m_Ontimer = true;

                    // 게임을 시작한다.
                    for (auto member : m_playerGroups[GroupID])
                    {
                        if (member != HostID_Server) // 서버는 제외
                        {
                            m_proxy.GameStart((HostID)member, RmiContext::ReliableSend);
                        }
                    }

                }
               
            }
        }

        // 플레이어 삭제
        m_Client_Infos.erase(info->m_HostID);
    };

    //proxy와 stub 객체를 NetServer에 부착
    m_server->AttachProxy(&m_proxy);
    m_server->AttachStub(this);

    // 서버 config
    CStartServerParameter startConfig; // config 객체
    startConfig.m_protocolVersion = g_protocolVersion; // 프로토콜 버전
    startConfig.m_tcpPorts.Add(g_serverPort); // TCP
    startConfig.m_udpPorts.Add(g_serverPort); // UDP
    startConfig.m_allowServerAsP2PGroupMember = true; // 서버도 P2P 그룹에 추가. 클라이언트 데이터 업데이트 때문에. 

    // 서버는 별도의 thread pool이 있으며 여기서 메시지 수신이나 이벤트가 실행.
    m_server->Start(startConfig); // 서버 시작

    // P2P HOSTID 생성
    HostID newP2PID = m_server->CreateP2PGroup();

    // P2P 방 정보 입력
    auto newGroupInfo = make_shared<RoomInfo>();
    newGroupInfo->m_player_num = 0; // 방 인원 수는 0명
    newGroupInfo->m_S_Team_num = 0; // 사파이어 팀의 인원 수는 0명
    newGroupInfo->m_R_Team_num = 0; // 루비 팀의 인원 수는 0명
    newGroupInfo->m_S_Team_Kill = 0; // 사파이어 팀 킬 수 초기화
    newGroupInfo->m_R_Team_Kill = 0; // 루비 팀 킬 수 초기화
    newGroupInfo->m_ready_game = false; // 게임 준비 여부 초기화
    newGroupInfo->m_end_game = false; // 게임 종료 여부 초기화
    newGroupInfo->m_In_game_num = 0; // 게임 준비 후 입장한 플레이어의 인원 수
    newGroupInfo->m_Ontimer = false; // 타이머 초기화
    newGroupInfo->m_Min = 0; // 시간 초기화
    newGroupInfo->m_Sec = 0;

    // 씨드값으로 현재의 서버의 시간(초)를 입력한다.
    srand((unsigned int)time(0));
    
    // 0 ~ 2의 난수
    int Randomindex = rand() % 3;

    // 날씨 저장
    newGroupInfo->m_weather = weathers[Randomindex];

    // 랜덤을 뽑기위한 temp
    vector<int> m_temp{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    // 5가지를 추출한다.
    for (int i = 0; i < 5; i++)
    {
        // 10개의 숫자 중 하나를 랜덤으로 뽑고 삭제.

        // m_temp에서 랜덤 인덱스 추출
        int ran_num = rand() % m_temp.size();
        
        // m_temp에서 추출한 중복 없는 난수
        int num = m_temp.at(ran_num);

        // m_temp의 해당 원소는 삭제
        m_temp.erase(m_temp.begin() + ran_num);

        newGroupInfo->m_items_x[i] = m_Total_item_x[num];
        newGroupInfo->m_items_y[i] = m_Total_item_y[num];
        newGroupInfo->m_items_z[i] = m_Total_item_z[num];

    }

    m_Group_Infos[newP2PID] = newGroupInfo;

    // P2P 방 생성

    // 서버 P2P Join (* 클라이언트 정보 업데이트를 위해서)
    // 대기방에 들어오는 클라이언트에게 다른 클라이언트 정보 업데이트할 때
    m_playerGroups[newP2PID].Add(HostID_Server);

    m_server->JoinP2PGroup(HostID_Server, newP2PID);

    // Hit return to exit
    cout << "Server started. Hit return to exit.\n";
    
    string line;
    getline(std::cin, line);
}

DEFRMI_S2C2S_RequestLogin(Aquarium_server)
{
    cout << "RequestLogin " << StringT2A(id.c_str()).GetString() << " "
        << StringT2A(password.c_str()).GetString() << endl;

    CAdoConnection conn;
    CAdoCommand cmd;

    // DB 연결
    conn.Open(L"server=.;database=Aquarium_DB;trusted_connection=yes", DbmsType::MsSql);

    //  GetGameUser 프로시저를 준비하고
    cmd.Prepare(conn, L"GetGameUser");
    
    // 해당 데이터를 기입한다.
    cmd.AppendParameter(L"UserID", ADODB::adVarWChar, ADODB::adParamInput, id.c_str());
    CAdoRecordset rs;
    cmd.Execute(rs); // 프로시저 실행

    // 해당 아이디가 없으면 리턴.
    if (rs.IsEOF())
    {
        cout << "dasda" << endl;
        m_proxy.NotifyLoginFailed(remote, RmiContext::ReliableSend, L"Invalid user ID");
        return true;
    }

    // 해당 아이디의 비밀번호가 틀리면 리턴.
    if (rs.GetFieldValue(L"UserID").IsNull())
    {
        cout << "dasda" << endl;
        m_proxy.NotifyLoginFailed(remote, RmiContext::ReliableSend, L"Invalid user ID");
        return true;
    }

    // 해당 비밀번호가 다르거나 없으면 리턴
    String password2;
    if (!rs.GetFieldValue(L"Password", password2) || password != password2.GetString())
    {
        cout << "asdasd" << endl;
        m_proxy.NotifyLoginFailed(remote, RmiContext::ReliableSend, L"Invalid password");
        return true;
    }

    // 잠금 
    CriticalSectionLock lock(m_critSec, true);

    // 이미 있는 유저인가?
    auto it = m_Client_Infos.find(remote);
    if (it != m_Client_Infos.end())
    {
        // 있으면 로그인 실패 처리
        m_proxy.NotifyLoginFailed(remote, RmiContext::ReliableSend, L"Already logged in.");
        return true;
    }

    // 플레이어 정보 추가하기
    auto newRC = make_shared<RemoteClient>();
    newRC->m_userID = id; // key 값은 Hostid
    newRC->m_groupID = 0; // 그룹 ID값은 0으로 초기화
    m_Client_Infos[remote] = newRC;

    // 로그인 성공!
    m_proxy.NotifyLoginSuccess(remote, RmiContext::ReliableSend);
    return true;

}

DEFRMI_S2C2S_Get_selete(Aquarium_server)
{
    cout << "Get_selete is called.\n";

    // 잠금 
    CriticalSectionLock lock(m_critSec, true);

    // 현재 시간 생성
    time_t curr_time = time(NULL);

    // 시간 표시를 위한 구조체를 선언.
    struct tm curr_tm;

    localtime_s(&curr_tm, &curr_time);

    int Now_Min = curr_tm.tm_min;
    int Now_Sec = curr_tm.tm_sec;

    // 게임 방 검색
    for (auto it = m_Group_Infos.begin(); it != m_Group_Infos.end(); ++it)
    {
        // 타이머가 즉, 게임이 시작된 방 중에서
        if (it->second->m_Ontimer)
        {
            // 게임이 종료하지 않은 방에서 
            if (!it->second->m_end_game)
            {
                // 접속한 인원이 최대 입장 가능 인원 수보다 작을 경우
                if (it->second->m_player_num < max_player_num)
                {
                    if (Now_Min < it->second->m_Min)
                        Now_Min += 60;

                    if (Now_Sec < it->second->m_Sec)
                    {
                        Now_Sec += 60;
                        Now_Min -= 1;
                    }

                    // 시간 결과
                    int Result_Min = Now_Min - it->second->m_Min;
                    int Result_Sec = Now_Sec - it->second->m_Sec;

                    int Sum = Result_Min * 60 + Result_Sec;
                    Sum = 600 - Sum;

                    // 게임이 5분 이상 남았을 경우
                    if (Sum >= 300)
                    {
                        // 해당 방에 입장할 여부를 물어본다.
                        m_proxy.Set_selete(remote, RmiContext::ReliableSend, 1, it->first, Sum / 60, Sum % 60);
                        return true;
                    }
                }
            }
        }
    }

    // 해당 조건에 만족하는 방이 없거나 존재하지 않으므로 방으로 간다.
    m_proxy.Set_selete(remote, RmiContext::ReliableSend, 0, 0, 0, 0);
    return true;


    // m_onTimer true -> m_player_num < max_player_num 있으면 -> select 전송 (시간, 해당 방 ID)
    // 없으면 해당게임으로 접속 JoinGameRoom();
    // ( result -> 0 : 없음, 1 : 있음 )
    // ( result가 1일 경우, Min, Sec 전송 )

    // select -> yes -> 인게임으로 접속 JoinINGame()
    // select -> no -> 해당게임으로 접속 JoinGameRoom()

}

DEFRMI_S2C2S_JoinGameRoom(Aquarium_server)
{
    cout << "JoinGameRoom is called.\n";

    // 잠금 
    CriticalSectionLock lock(m_critSec, true);

    // 클라이언트 데이터에 있는지 확인
    auto it = m_Client_Infos.find(remote);
    if (it == m_Client_Infos.end())
    {
        // 없다면 리턴
        return true;
    }

    //  클라이언트 정보 업데이트
    auto& rc = it->second;

    rc->m_character = static_cast<MyCharacter>(character_num);

    // P2P 방 Key
    HostID RoomID;

    // 플레이어 방 검색, 게임 방 조인
    for(auto it = m_Group_Infos.begin(); it != m_Group_Infos.end(); ++it)
    {
        cout << "P2P 정보" << it->first << endl;

        // 게임 시작하지 않은 게임방 중에서
        if (!it->second->m_Ontimer)
        {
            // 인원 수가 최대 인원 수보다 적을 경우 입장
            if (it->second->m_player_num < max_player_num)
            {
                cout << "인원 수가 적음. " << endl;

                RoomID = HostID(it->first);

                // P2P 그룹에 추가
                // 해당 클라이언트는 P2PMemberJoinHandler 이벤트 콜백
                m_playerGroups[RoomID].Add(remote);

                m_server->JoinP2PGroup(remote, RoomID);

                // 인원 수 증가
                it->second->m_player_num++;

                // P2PGroup ID 저장
                rc->m_groupID = RoomID;
                rc->m_Response = false;

                break;
            }
        }

        // 마지막 방이고 인원 수가 모두 찼을 경우
        if (it == --m_Group_Infos.end())
        {
            cout << "새로운 방 생성 " << endl;

            // P2P 방 생성
            RoomID = m_server->CreateP2PGroup();

            // P2P 방 정보 입력
            auto newGroupInfo = make_shared<RoomInfo>();
            newGroupInfo->m_player_num = 1; // 방 인원수는 1명, 이제 입장할 예정이므로
            newGroupInfo->m_S_Team_num = 0; // 사파이어 팀의 인원 수는 0명
            newGroupInfo->m_R_Team_num = 0; // 루비 팀의 인원 수는 0명
            newGroupInfo->m_S_Team_Kill = 0; // 사파이어 팀 킬 수 초기화
            newGroupInfo->m_R_Team_Kill = 0; // 루비 팀 킬 수 초기화
            newGroupInfo->m_ready_game = false; // 게임 준비 여부 초기화
            newGroupInfo->m_end_game = false; // 게임 종료 여부 초기화
            newGroupInfo->m_In_game_num = 0; // 게임 준비 후 입장한 플레이어의 인원 수
            newGroupInfo->m_Ontimer = false; // 타이머 초기화
            newGroupInfo->m_Min = 0; // 시간 초기화
            newGroupInfo->m_Sec = 0;

            // 씨드값으로 현재의 서버의 시간(초)를 입력한다.
            srand((unsigned int)time(0));

            // 0 ~ 2의 난수
            int Randomindex = rand() % 3;

            // 날씨 저장
            newGroupInfo->m_weather = weathers[Randomindex];

            // 랜덤을 뽑기위한 temp
            vector<int> m_temp{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            // 5가지를 추출한다.
            for (int i = 0; i < 5; i++)
            {
                // 10개의 숫자 중 하나를 랜덤으로 뽑고 삭제.

                // m_temp에서 랜덤 인덱스 추출
                int ran_num = rand() % m_temp.size();

                // m_temp에서 추출한 중복 없는 난수
                int num = m_temp.at(ran_num);

                // m_temp의 해당 원소는 삭제
                m_temp.erase(m_temp.begin() + ran_num);

                newGroupInfo->m_items_x[i] = m_Total_item_x[num];
                newGroupInfo->m_items_y[i] = m_Total_item_y[num];
                newGroupInfo->m_items_z[i] = m_Total_item_z[num];

            }

            m_Group_Infos[RoomID] = newGroupInfo;

            // P2P 방 생성

            // 서버 P2P Join (* 클라이언트 정보 업데이트를 위해서)
            // 대기방에 들어오는 클라이언트에게 다른 클라이언트 정보 업데이트할 때
            m_playerGroups[RoomID].Add(HostID_Server);

            // 해당 클라이언트는 P2PMemberJoinHandler 이벤트 콜백
            m_playerGroups[RoomID].Add(remote);

            m_server->JoinP2PGroup(HostID_Server, RoomID);
            m_server->JoinP2PGroup(remote, RoomID);

            // P2PGroup ID 저장
            rc->m_groupID = RoomID;
            rc->m_Response = false;

            break;
        }
    }

    // 만약 서버 시작시에 방이 생성되어 1개 이상이지만 그 마저 없다면.
    if (m_Group_Infos.empty())
    {
        cout << "아무것도 없기에 새로운 방 생성 " << endl;

        // P2P 방 생성
        RoomID = m_server->CreateP2PGroup();

        // P2P 방 정보 입력
        auto newGroupInfo = make_shared<RoomInfo>();
        newGroupInfo->m_player_num = 1; // 방 인원수는 1명, 이제 입장할 예정이므로
        newGroupInfo->m_S_Team_num = 0; // 사파이어 팀의 인원 수는 0명
        newGroupInfo->m_R_Team_num = 0; // 루비 팀의 인원 수는 0명
        newGroupInfo->m_S_Team_Kill = 0; // 사파이어 팀 킬 수 초기화
        newGroupInfo->m_R_Team_Kill = 0; // 루비 팀 킬 수 초기화
        newGroupInfo->m_ready_game = false; // 게임 준비 여부 초기화
        newGroupInfo->m_end_game = false; // 게임 종료 여부 초기화
        newGroupInfo->m_In_game_num = 0; // 게임 준비 후 입장한 플레이어의 인원 수
        newGroupInfo->m_Ontimer = false; // 타이머 초기화
        newGroupInfo->m_Min = 0; // 시간 초기화
        newGroupInfo->m_Sec = 0;

        // 씨드값으로 현재의 서버의 시간(초)를 입력한다.
        srand((unsigned int)time(0));

        // 0 ~ 2의 난수
        int Randomindex = rand() % 3;

        // 날씨 저장
        newGroupInfo->m_weather = weathers[Randomindex];

        // 랜덤을 뽑기위한 temp
        vector<int> m_temp{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

        // 5가지를 추출한다.
        for (int i = 0; i < 5; i++)
        {
            // 10개의 숫자 중 하나를 랜덤으로 뽑고 삭제.

            // m_temp에서 랜덤 인덱스 추출
            int ran_num = rand() % m_temp.size();

            // m_temp에서 추출한 중복 없는 난수
            int num = m_temp.at(ran_num);

            // m_temp의 해당 원소는 삭제
            m_temp.erase(m_temp.begin() + ran_num);

            newGroupInfo->m_items_x[i] = m_Total_item_x[num];
            newGroupInfo->m_items_y[i] = m_Total_item_y[num];
            newGroupInfo->m_items_z[i] = m_Total_item_z[num];

        }

        m_Group_Infos[RoomID] = newGroupInfo;

        // P2P 방 생성

        // 서버 P2P Join (* 클라이언트 정보 업데이트를 위해서)
        // 대기방에 들어오는 클라이언트에게 다른 클라이언트 정보 업데이트할 때
        m_playerGroups[RoomID].Add(HostID_Server);

        // 해당 클라이언트는 P2PMemberJoinHandler 이벤트 콜백
        m_playerGroups[RoomID].Add(remote);

        m_server->JoinP2PGroup(HostID_Server, RoomID);
        m_server->JoinP2PGroup(remote, RoomID);

        // P2PGroup ID 저장
        rc->m_groupID = RoomID;
        rc->m_Response = false;
    }

    int S_num = m_Group_Infos[RoomID]->m_S_Team_num;
    int R_num = m_Group_Infos[RoomID]->m_R_Team_num;

    wstring TeamColor; // 팀 색상

    // 플레이어 팀 색상 및 번호 부여
    if (S_num > R_num) // 2 : 1 or 1 : 0 or 2: 0 ( 루비 팀 부여 )
    {
        // 플레이어 팀 업데이트 및 인원 추가 업데이트
        TeamColor = L"Ruby";
        rc->m_Team = L"Ruby";
        m_Group_Infos[RoomID]->m_R_Team_num++;
        
    }
    else if (S_num < R_num) // 1 : 2 or 0 : 1 or 0 : 2 ( 사파이어 팀 부여 )
    {
        // 플레이어 팀 업데이트 및 인원 추가 업데이트
        TeamColor = L"Sapphire";
        rc->m_Team = L"Sapphire";
        m_Group_Infos[RoomID]->m_S_Team_num++;

    }
    else if (S_num == R_num) // 0 : 0 or 1 : 1 ( 사파이어 팀 부여 )
    {
        // 플레이어 팀 업데이트 및 인원 추가 업데이트
        TeamColor = L"Sapphire";
        rc->m_Team = L"Sapphire";
        m_Group_Infos[RoomID]->m_S_Team_num++;

    }

    // 루비 팀일 경우에는 2번 혹은 4번
    if (TeamColor.compare(L"Ruby") == 0)
    {
        if (m_Group_Infos[RoomID]->m_R_Team_num == 1) // 본인 클라이언트만 있을 경우
        {
            m_Client_Infos[remote]->m_TeamNum = 2; // 2번 즉, 맨 앞

            // ** 리스폰 좌표 설정 **
            
            // 위치
            rc->m_R_posX = -112;
            rc->m_R_posY = 0;
            rc->m_R_posZ = 85;

            // 방향
            rc->m_R_rotX = 0;
            rc->m_R_rotY = -237;
            rc->m_R_rotZ = 0;

        }

        else // 같은 팀원이 있을 경우
        {
            for (auto it : m_playerGroups[RoomID]) // 같은 방 팀원의 ID에서
            {
                if (it != HostID_Server && m_Client_Infos[it]->m_Team.compare(L"Ruby") == 0) // 같은 팀원의 ID를 찾자.
                {
                    if (m_Client_Infos[it]->m_TeamNum == 2) // 같은 팀원의 번호가 2일 경우
                    {
                        m_Client_Infos[remote]->m_TeamNum = 4; // 본인은 4번으로

                        // ** 리스폰 좌표 설정 **

                        // 위치
                        rc->m_R_posX = -112;
                        rc->m_R_posY = 0;
                        rc->m_R_posZ = 80;

                        // 방향
                        rc->m_R_rotX = 0;
                        rc->m_R_rotY = -290;
                        rc->m_R_rotZ = 0;

                        break;
                    }
                    else if (m_Client_Infos[it]->m_TeamNum == 4) // 같은 팀원의 번호가 4일 경우
                    {
                        m_Client_Infos[remote]->m_TeamNum = 2; // 본인은 2번으로

                        // ** 리스폰 좌표 설정 **

                        // 위치
                        rc->m_R_posX = -112;
                        rc->m_R_posY = 0;
                        rc->m_R_posZ = 85;

                        // 방향
                        rc->m_R_rotX = 0;
                        rc->m_R_rotY = -237;
                        rc->m_R_rotZ = 0;


                        break;
                    }
                }
            }
        }
    }

    // 사파이어 팀일 경우에는 1번 혹은 3번
    if (TeamColor.compare(L"Sapphire") == 0)
    {
        if (m_Group_Infos[RoomID]->m_S_Team_num == 1) // 본인 클라이언트만 있을 경우
        {
            m_Client_Infos[remote]->m_TeamNum = 1; // 1번 즉, 맨 앞

            // ** 리스폰 좌표 설정 **

            // 위치
            rc->m_R_posX = -32;
            rc->m_R_posY = 0;
            rc->m_R_posZ = 58;

            // 방향
            rc->m_R_rotX = 0;
            rc->m_R_rotY = -344;
            rc->m_R_rotZ = 0;
        }

        else // 같은 팀원이 있을 경우
        {
            for (auto it : m_playerGroups[RoomID]) // 같은 방 팀원의 ID에서
            {
                if (it != HostID_Server && m_Client_Infos[it]->m_Team.compare(L"Sapphire") == 0) // 같은 팀원의 ID를 찾자.
                {
                    if (m_Client_Infos[it]->m_TeamNum == 1) // 같은 팀원의 번호가 1일 경우
                    {
                        m_Client_Infos[remote]->m_TeamNum = 3; // 본인은 3번으로

                        // ** 리스폰 좌표 설정 **

                        // 위치
                        rc->m_R_posX = -28;
                        rc->m_R_posY = 0;
                        rc->m_R_posZ = 58;

                        // 방향
                        rc->m_R_rotX = 0;
                        rc->m_R_rotY = -368;
                        rc->m_R_rotZ = 0;

                        break;
                    }
                    else if (m_Client_Infos[it]->m_TeamNum == 3) // 같은 팀원의 번호가 3일 경우
                    {
                        m_Client_Infos[remote]->m_TeamNum = 1; // 본인은 1번으로

                        // ** 리스폰 좌표 설정 **

                        // 위치
                        rc->m_R_posX = -32;
                        rc->m_R_posY = 0;
                        rc->m_R_posZ = 58;

                        // 방향
                        rc->m_R_rotX = 0;
                        rc->m_R_rotY = -344;
                        rc->m_R_rotZ = 0;

                        break;
                    }
                }
            }
        }
    }

    // 그 외 초기화
    rc->m_humidity = 0; // 습도량

    // it의 key 값이 remote와 다를 경우 에러 발생.
    assert(it->first == remote);

    // 받은 플레이어 캐릭터 정보를 업데이트한다.
    // 기존 클라이언트 목록을 순회하면서, 기존 클라이언트에게 새 클라이언트의 등장을 알림.
    // 마찬가지로, 새 클라이언트에게 기존 클라이언트의 등장을 알림.

        // 팀 방에 입장하고 플레이어 UI 처리
    for (auto member : m_playerGroups[RoomID])
    {
        if (member != HostID_Server) // 서버는 제외
        {

            auto& other = m_Client_Infos.find(member)->second; // 멤버 클라이언트 정보

            if (member != it->first) // 기존 클라이언트일 경우
            {

                // 기존 클라이언트에게 새 클라이언트를 알림
                m_proxy.Room_Appear((HostID)member, RmiContext::ReliableSend,
                    (HostID)it->first, rc->m_userID, rc->m_character, rc->m_Team, rc->m_TeamNum);

                // 새 클라이언트에게 기존 클라이언트를 알림
                m_proxy.Room_Appear((HostID)it->first, RmiContext::ReliableSend,
                    (HostID)member, other->m_userID, other->m_character, other->m_Team, other->m_TeamNum);
            }

            else // 새 클라이언트일 경우
            {
                // 새 클라이언트에게 새 클라이언트의 정보 업데이트
                m_proxy.Room_Appear((HostID)it->first, RmiContext::ReliableSend,
                    (HostID)it->first, rc->m_userID, rc->m_character, rc->m_Team, rc->m_TeamNum);
            }

            // 팀 인원 수가 최대 인원수로 될 경우, 게임 카운트 시작.
            if (m_Group_Infos[RoomID]->m_player_num == max_player_num)
            {
                // 게임 카운트 시작.
                m_proxy.CountStart((HostID)member, RmiContext::ReliableSend);

                // 게임 준비 중임을 갱신.
                m_Group_Infos[RoomID]->m_ready_game = true;
            }
        }
    }

    // 1. 본인 게임 캐릭터의 리스폰 정보와 방 정보를 줌.
    // 2. 다른 게임 캐릭터의 리스폰 정보를 줌.

    // 방에 있는 멤버들에게
    for (auto member : m_playerGroups[RoomID])
    {
        if (member != HostID_Server) // 서버는 제외
        {
            auto& my = m_Client_Infos.find(member)->second; // 멤버 클라이언트 정보

            // 본인 캐릭터의 리스폰 정보를 준다.
            m_proxy.PlayerInfo((HostID)member, RmiContext::ReliableSend, my->m_TeamNum, my->m_character,
                my->m_R_posX, my->m_R_posY, my->m_R_posZ,
                my->m_R_rotX, my->m_R_rotY, my->m_R_rotZ);

            // 플레이어의 체력 전송
            m_proxy.Player_GetHP((HostID)member, RmiContext::ReliableSend, my->m_humidity);

            // 방 날씨 전송
            m_proxy.Room_weather((HostID)member, RmiContext::ReliableSend, m_Group_Infos[RoomID]->m_weather);

            // 방 아이템 정보 전송
            for (int i = 0; i < 5; ++i)
            {
                m_proxy.Room_Item((HostID)member, RmiContext::ReliableSend, i, m_Group_Infos[RoomID]->m_items_x[i], m_Group_Infos[RoomID]->m_items_y[i], m_Group_Infos[RoomID]->m_items_z[i]);
            }

            // 방에 있는 나를 제외한 멤버들에게
            for (auto other_member : m_playerGroups[RoomID])
            {
                if (member != HostID_Server && member != other_member) // 서버와 본인 제외
                {
                    // 본인 캐릭터의 리스폰 정보를 준다.
                    m_proxy.PlayerInfo((HostID)other_member, RmiContext::ReliableSend, my->m_TeamNum, my->m_character,
                        my->m_R_posX, my->m_R_posY, my->m_R_posZ,
                        my->m_R_rotX, my->m_R_rotY, my->m_R_rotZ);
                }
            }
        }
    }

    // 3명이 전부 들어가고 1명이 들어오지 않고 나감. #1
    // 2명 들어오고 1명 나가고 이후 1명 들어옴. #2
    // 모든 유저가 게임 입장을 시작하면 m_ready_game = true;
    // 유저가 입장하면 m_In_game = true;
    // 입장 한 요청을 서버에 보내면 전부 조사해서 m_player_num과 m_In_game 여부 인원 수와 같은지 확인 후 게임시작 #1
    // 누군가 나가면 게임 방이 m_ready_game = true; 면 확인 후 게임시작 #2

    // 무적임을 서버에 알려야 함.
    
    // 중간에 들어오면 서버에서 타이머 시간, 팀 점수, 방 날씨, 체력, 아이템 정보 전달
    // 클라에서는 리스폰에서 스폰, 모든 값 초기화

    // 중간에 퇴장했을 때 해당 방 m_Ontimer 여부를 통해서 퇴장 처리 가능.

    return true;

}

DEFRMI_S2C2S_JoinInGame(Aquarium_server)
{
    cout << "JoinInGame is called.\n";

    // 잠금 
    CriticalSectionLock lock(m_critSec, true);

    // 클라이언트 데이터에 있는지 확인
    auto it = m_Client_Infos.find(remote);
    if (it == m_Client_Infos.end())
    {
        // 없다면 리턴
        return true;
    }

    //  클라이언트 정보 업데이트
    auto& rc = it->second;

    rc->m_character = static_cast<MyCharacter>(character_num);

    // P2P 방 Key
    HostID RoomID = static_cast<HostID>(Room_id);

    cout << "RoomID : " << RoomID << endl;

    // P2P 그룹에 추가
    // 해당 클라이언트는 P2PMemberJoinHandler 이벤트 콜백
    m_playerGroups[RoomID].Add(remote);

    m_server->JoinP2PGroup(remote, RoomID);

    // 인원 수 증가
    m_Group_Infos[RoomID]->m_player_num++;

    // P2PGroup ID 저장
    rc->m_groupID = RoomID;
    rc->m_Response = false;

    int S_num = m_Group_Infos[RoomID]->m_S_Team_num;
    int R_num = m_Group_Infos[RoomID]->m_R_Team_num;

    wstring TeamColor; // 팀 색상

    // 플레이어 팀 색상 및 번호 부여
    if (S_num > R_num) // 2 : 1 or 1 : 0 or 2: 0 ( 루비 팀 부여 )
    {
        // 플레이어 팀 업데이트 및 인원 추가 업데이트
        TeamColor = L"Ruby";
        rc->m_Team = L"Ruby";
        m_Group_Infos[RoomID]->m_R_Team_num++;

    }
    else if (S_num < R_num) // 1 : 2 or 0 : 1 or 0 : 2 ( 사파이어 팀 부여 )
    {
        // 플레이어 팀 업데이트 및 인원 추가 업데이트
        TeamColor = L"Sapphire";
        rc->m_Team = L"Sapphire";
        m_Group_Infos[RoomID]->m_S_Team_num++;

    }
    else if (S_num == R_num) // 0 : 0 or 1 : 1 ( 사파이어 팀 부여 )
    {
        // 플레이어 팀 업데이트 및 인원 추가 업데이트
        TeamColor = L"Sapphire";
        rc->m_Team = L"Sapphire";
        m_Group_Infos[RoomID]->m_S_Team_num++;

    }

    // 루비 팀일 경우에는 2번 혹은 4번
    if (TeamColor.compare(L"Ruby") == 0)
    {
        if (m_Group_Infos[RoomID]->m_R_Team_num == 1) // 본인 클라이언트만 있을 경우
        {
            m_Client_Infos[remote]->m_TeamNum = 2; // 2번 즉, 맨 앞

            // ** 리스폰 좌표 설정 **

            // 위치
            rc->m_R_posX = -112;
            rc->m_R_posY = 0;
            rc->m_R_posZ = 85;

            // 방향
            rc->m_R_rotX = 0;
            rc->m_R_rotY = -237;
            rc->m_R_rotZ = 0;

        }

        else // 같은 팀원이 있을 경우
        {
            for (auto it : m_playerGroups[RoomID]) // 같은 방 팀원의 ID에서
            {
                if (it != HostID_Server && m_Client_Infos[it]->m_Team.compare(L"Ruby") == 0) // 같은 팀원의 ID를 찾자.
                {
                    if (m_Client_Infos[it]->m_TeamNum == 2) // 같은 팀원의 번호가 2일 경우
                    {
                        m_Client_Infos[remote]->m_TeamNum = 4; // 본인은 4번으로

                        // ** 리스폰 좌표 설정 **

                        // 위치
                        rc->m_R_posX = -112;
                        rc->m_R_posY = 0;
                        rc->m_R_posZ = 80;

                        // 방향
                        rc->m_R_rotX = 0;
                        rc->m_R_rotY = -290;
                        rc->m_R_rotZ = 0;

                        break;
                    }
                    else if (m_Client_Infos[it]->m_TeamNum == 4) // 같은 팀원의 번호가 4일 경우
                    {
                        m_Client_Infos[remote]->m_TeamNum = 2; // 본인은 2번으로

                        // ** 리스폰 좌표 설정 **

                        // 위치
                        rc->m_R_posX = -112;
                        rc->m_R_posY = 0;
                        rc->m_R_posZ = 85;

                        // 방향
                        rc->m_R_rotX = 0;
                        rc->m_R_rotY = -237;
                        rc->m_R_rotZ = 0;


                        break;
                    }
                }
            }
        }
    }

    // 사파이어 팀일 경우에는 1번 혹은 3번
    if (TeamColor.compare(L"Sapphire") == 0)
    {
        if (m_Group_Infos[RoomID]->m_S_Team_num == 1) // 본인 클라이언트만 있을 경우
        {
            m_Client_Infos[remote]->m_TeamNum = 1; // 1번 즉, 맨 앞

            // ** 리스폰 좌표 설정 **

            // 위치
            rc->m_R_posX = -32;
            rc->m_R_posY = 0;
            rc->m_R_posZ = 58;

            // 방향
            rc->m_R_rotX = 0;
            rc->m_R_rotY = -344;
            rc->m_R_rotZ = 0;
        }

        else // 같은 팀원이 있을 경우
        {
            for (auto it : m_playerGroups[RoomID]) // 같은 방 팀원의 ID에서
            {
                if (it != HostID_Server && m_Client_Infos[it]->m_Team.compare(L"Sapphire") == 0) // 같은 팀원의 ID를 찾자.
                {
                    if (m_Client_Infos[it]->m_TeamNum == 1) // 같은 팀원의 번호가 1일 경우
                    {
                        m_Client_Infos[remote]->m_TeamNum = 3; // 본인은 3번으로

                        // ** 리스폰 좌표 설정 **

                        // 위치
                        rc->m_R_posX = -28;
                        rc->m_R_posY = 0;
                        rc->m_R_posZ = 58;

                        // 방향
                        rc->m_R_rotX = 0;
                        rc->m_R_rotY = -368;
                        rc->m_R_rotZ = 0;

                        break;
                    }
                    else if (m_Client_Infos[it]->m_TeamNum == 3) // 같은 팀원의 번호가 3일 경우
                    {
                        m_Client_Infos[remote]->m_TeamNum = 1; // 본인은 1번으로

                        // ** 리스폰 좌표 설정 **

                        // 위치
                        rc->m_R_posX = -32;
                        rc->m_R_posY = 0;
                        rc->m_R_posZ = 58;

                        // 방향
                        rc->m_R_rotX = 0;
                        rc->m_R_rotY = -344;
                        rc->m_R_rotZ = 0;

                        break;
                    }
                }
            }
        }
    }


    // it의 key 값이 remote와 다를 경우 에러 발생.
    assert(it->first == remote);

    // 받은 플레이어 캐릭터 정보를 업데이트한다.
    // 기존 클라이언트 목록을 순회하면서, 기존 클라이언트에게 새 클라이언트의 등장을 알림.
    // 마찬가지로, 새 클라이언트에게 기존 클라이언트의 등장을 알림.

    // 현재 시간 생성
    time_t curr_time = time(NULL);

    // 시간 표시를 위한 구조체를 선언.
    struct tm curr_tm;

    localtime_s(&curr_tm, &curr_time);

    int Now_Min = curr_tm.tm_min;
    int Now_Sec = curr_tm.tm_sec;

    if (Now_Min < m_Group_Infos[RoomID]->m_Min)
        Now_Min += 60;

    if (Now_Sec < m_Group_Infos[RoomID]->m_Sec)
    {
        Now_Sec += 60;
        Now_Min -= 1;
    }

    // 시간 결과
    int Result_Min = Now_Min - m_Group_Infos[RoomID]->m_Min;
    int Result_Sec = Now_Sec - m_Group_Infos[RoomID]->m_Sec;

    int Sum = Result_Min * 60 + Result_Sec;
    Sum = 600 - Sum;

    // 게임 입장을 준비한다.
    m_proxy.Game_Appear(remote, RmiContext::ReliableSend, 1, rc->m_TeamNum, Sum / 60, Sum % 60, m_Group_Infos[RoomID]->m_S_Team_Kill, m_Group_Infos[RoomID]->m_R_Team_Kill);

    // 본인 캐릭터의 리스폰 정보를 준다.
    m_proxy.PlayerInfo(remote, RmiContext::ReliableSend, rc->m_TeamNum, rc->m_character,
        rc->m_R_posX, rc->m_R_posY, rc->m_R_posZ,
        rc->m_R_rotX, rc->m_R_rotY, rc->m_R_rotZ);

    // 플레이어의 체력 전송
    m_proxy.Player_GetHP(remote, RmiContext::ReliableSend, rc->m_humidity);

    // 방 날씨 전송
    m_proxy.Room_weather(remote, RmiContext::ReliableSend, m_Group_Infos[RoomID]->m_weather);

    // 방 아이템 정보 전송
    for (int i = 0; i < 5; ++i)
    {
        m_proxy.Room_Item(remote, RmiContext::ReliableSend, i, m_Group_Infos[RoomID]->m_items_x[i], m_Group_Infos[RoomID]->m_items_y[i], m_Group_Infos[RoomID]->m_items_z[i]);
    }

    // 본인에게 다른 캐릭터의 리스폰 정보를 준다.
    for (auto other_member : m_playerGroups[RoomID])
    {
        if (other_member != HostID_Server && remote != other_member) // 서버와 본인 제외
        {
            auto& other = m_Client_Infos.find(other_member)->second; // 멤버 클라이언트 정보

            m_proxy.PlayerInfo(remote, RmiContext::ReliableSend, other->m_TeamNum, other->m_character,
                other->m_R_posX, other->m_R_posY, other->m_R_posZ,
                other->m_R_rotX, other->m_R_rotY, other->m_R_rotZ);

            m_proxy.PlayerInfo((HostID)other_member, RmiContext::ReliableSend, rc->m_TeamNum, rc->m_character,
                rc->m_R_posX, rc->m_R_posY, rc->m_R_posZ,
                rc->m_R_rotX, rc->m_R_rotY, rc->m_R_rotZ);
        }
    }

    return true;
}

DEFRMI_S2C2S_LeaveGameRoom(Aquarium_server)
{
    cout << "LeaveGameRoom is called.\n";

    // 잠금 
    CriticalSectionLock lock(m_critSec, true);

    // 클라이언트 데이터에 있는지 확인
    auto it = m_Client_Infos.find(remote);
    if (it == m_Client_Infos.end())
    {
        // 없다면 리턴
        return true;
    }

    // P2P 방 아이디
    HostID GroupID = static_cast<HostID>(m_Client_Infos[remote]->m_groupID);

    // it의 key 값이 remote와 다를 경우 에러 발생.
    assert(it->first == remote);

    // 클라이언트의 그룹아이디 값 초기화
    m_Client_Infos[remote]->m_groupID = 0;

    // P2P 방에서 클라이언트 삭제
    m_server->LeaveP2PGroup(remote, GroupID);

    // 만약 방에 나만 있었다면, P2P 방을 삭제.
    if (m_Group_Infos[GroupID]->m_player_num == 1)
    {
        // P2P 방 정보 및 데이터 삭제
        m_Group_Infos.erase(GroupID);
        m_playerGroups.erase(GroupID);

        m_server->DestroyP2PGroup(GroupID);
    }
    // 방에 나 이외에 다른 사람도 있었다면 P2P 방을 계속 보관.
    else if (m_Group_Infos[GroupID]->m_player_num > 1)
    {
        // 플레이어 감소
        m_Group_Infos[GroupID]->m_player_num--;

        if (m_Client_Infos[remote]->m_Team.compare(L"Sapphire") == 0)
        {
            m_Group_Infos[GroupID]->m_S_Team_num--;
        }
        if (m_Client_Infos[remote]->m_Team.compare(L"Ruby") == 0)
        {
            m_Group_Infos[GroupID]->m_R_Team_num--;
        }
            
        // 멤버 삭제
        m_playerGroups[GroupID].RemoveOneByValue(remote);

        // P2P 방에 있는 클라이언트에게 게임 방 퇴장 알림.
        for (auto member : m_playerGroups[GroupID])
        {
            m_proxy.Room_Disappear(member, RmiContext::ReliableSend, m_Client_Infos[remote]->m_TeamNum);
        }
    }

    return true;

}

DEFRMI_S2C2S_Player_Move(Aquarium_server)
{
    CriticalSectionLock lock(m_critSec, true);

    // 클라이언트 데이터에 있는지 확인
    auto it = m_Client_Infos.find(remote);
    if (it == m_Client_Infos.end())
    {
        // 없으면 리턴
        return true;
    }

    //  클라이언트 정보 업데이트
    auto& rc = it->second;

    rc->m_posX = px;
    rc->m_posY = py;
    rc->m_posZ = pz;
    rc->m_rotX = rx;
    rc->m_rotY = ry;
    rc->m_rotZ = rz;
    rc->m_move = m_move;
    rc->m_rotate = m_rotate;
    rc->reload = m_reload;

    return true;

}

DEFRMI_S2C2S_Player_SetHP(Aquarium_server)
{
    CriticalSectionLock lock(m_critSec, true);

    // 해당 플레이어 체력 데미지 주기

    // P2P 방 KEY 
    HostID RoomID = static_cast<HostID>(m_Client_Infos[remote]->m_groupID);

    for (auto member : m_playerGroups[RoomID])
    {
        if (member != HostID_Server) // 서버는 제외
        {
            auto& my = m_Client_Infos.find(member)->second; // 멤버 클라이언트 정보

            if (my->m_TeamNum == m_team_num)
            {
                // 무적 상태면 제외
                if (my->m_Response)     
                {
                    break; 
                }

                // 데미지를 준 후
                my->m_humidity += m_damage;

                if (my->m_Team.compare(L"Ruby") == 0) // 루비 팀
                {
                    if (my->m_humidity >= 120) // 습도가 초과하면
                    {
                        // 플레이어 킬 처리
                        m_proxy.Player_Kill((HostID)member, RmiContext::ReliableSend, my->m_TeamNum);
                        my->m_Response = true; // 무적상태로 만들고
                        my->m_humidity = 0; // 체력 초기화

                        // 루비팀이 죽었으므로 사파이어 킬
                        m_Group_Infos[RoomID]->m_S_Team_Kill++;

                        break;
                    }
                }
                else if (my->m_Team.compare(L"Sapphire") == 0) // 사파이어 팀
                {
                    if (my->m_humidity >= 100) // 습도가 초과하면
                    {
                        // 플레이어 킬 처리
                        m_proxy.Player_Kill((HostID)member, RmiContext::ReliableSend, my->m_TeamNum);
                        my->m_Response = true; // 무적상태로 만들고
                        my->m_humidity = 0; // 체력 초기화

                        // 사파이어팀이 죽었으므로 루비 킬
                        m_Group_Infos[RoomID]->m_R_Team_Kill++;

                        break;
                    }
                }

                // 플레이어의 체력 전송
                m_proxy.Player_GetHP((HostID)member, RmiContext::ReliableSend, my->m_humidity);
                break;
            }
        }
    }

    return true;
}

DEFRMI_S2C2S_Player_SetReady(Aquarium_server)
{
    cout << "Player_SetReady" << endl;

    CriticalSectionLock lock(m_critSec, true);

    // P2P 방 KEY 
    HostID RoomID = static_cast<HostID>(m_Client_Infos[remote]->m_groupID);

    // 입장 처리
    m_Client_Infos[remote]->m_Response = true; // 무적상태로 만들고
    m_Group_Infos[RoomID]->m_In_game_num++; // 게임 준비가 완료된 플레이어 인원 수를 늘리고

    // 입장한 정원이 모두 준비가 됐다면
    if (m_Group_Infos[RoomID]->m_In_game_num == m_Group_Infos[RoomID]->m_player_num)
    {
        if (m_Group_Infos[RoomID]->m_Ontimer)
            return true;

        // 타이머를 설정하고
        m_Group_Infos[RoomID]->m_Ontimer = true;

        // 현재 시간 생성
        time_t curr_time = time(NULL);

        // 시간 표시를 위한 구조체를 선언.
        struct tm curr_tm;
        
        // 게임을 시작한다.
        for (auto member : m_playerGroups[RoomID])
        {
            if (member != HostID_Server) // 서버는 제외
            {
                m_proxy.GameStart((HostID)member, RmiContext::ReliableSend);
            }
        }

        // 게임 시작 시간 저장
        localtime_s(&curr_tm, &curr_time);

        m_Group_Infos[RoomID]->m_Min = curr_tm.tm_min;
        m_Group_Infos[RoomID]->m_Sec = curr_tm.tm_sec;

    }

    return true;
}

DEFRMI_S2C2S_Player_SetResponse(Aquarium_server)
{
    cout << "Player_SetResponse value :" << value << endl;

    CriticalSectionLock lock(m_critSec, true);

    // 플레이어의 무적 여부 업데이트
    m_Client_Infos[remote]->m_Response = value;

    return true;
}

DEFRMI_S2C2S_Get_END(Aquarium_server)
{
    // 게임 방 종료 시키기
    cout << "Get_END" << endl;

    CriticalSectionLock lock(m_critSec, true);
    
    // P2P 방 KEY 
    HostID RoomID = static_cast<HostID>(m_Client_Infos[remote]->m_groupID);

    // 게임이 시작된 상태에서
    if (m_Group_Infos[RoomID]->m_Ontimer)
    {
        // 아직 종료 처리를 하지 않았다면
        if (!m_Group_Infos[RoomID]->m_end_game)
        {
            // 게임종료 처리를 하고
            m_Group_Infos[RoomID]->m_end_game = true;
            
            // 게임 결과를 계산한다.
            wstring game_result;

            if (m_Group_Infos[RoomID]->m_S_Team_Kill > m_Group_Infos[RoomID]->m_R_Team_Kill)
            {
                // 사파이어 승
                game_result = L"Sapphire";
            }
            else if (m_Group_Infos[RoomID]->m_S_Team_Kill < m_Group_Infos[RoomID]->m_R_Team_Kill)
            {
                // 루비 승
                game_result = L"Ruby";
            }
            else // 무승부인 경우 플레이어의 체력을 계산한다.
            {
                int R_sum = 0; // 루비팀 체력 합산
                int S_sum = 0; // 사파이어팀 체력 합산

                for (auto member : m_playerGroups[RoomID])
                {
                    if (member != HostID_Server) // 서버는 제외
                    {
                        auto& my = m_Client_Infos.find(member)->second; // 멤버 클라이언트 정보

                        if (my->m_Team.compare(L"Ruby") == 0) // 루비팀이면 루비팀에 추가
                        {
                            R_sum += my->m_humidity;
                        }
                        else if (my->m_Team.compare(L"Sapphire") == 0) // 사파이어팀이면 사파이어팀에 추가
                        {
                            S_sum += my->m_humidity;
                        }
                    }
                }

                //  점수로 판단
                if (S_sum < R_sum)
                {
                    // 사파이어 승
                    game_result = L"Sapphire";
                }
                else if (S_sum > R_sum)
                {
                    // 루비 승
                    game_result = L"Ruby";
                }
                else // 그래도 같다면 무승부 처리
                {
                    // 무승부
                    game_result = L"Draw";
                }

            }

            // 게임 결과를 전송한다.
            for (auto member : m_playerGroups[RoomID])
            {
                if (member != HostID_Server) // 서버는 제외
                {
                    m_proxy.Set_END((HostID)member, RmiContext::ReliableSend, game_result);
                }
            }

        }
    }

    return true;
}