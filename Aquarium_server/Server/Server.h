﻿#pragma once

#include "../Common/Common.h"
#include "../Common/S2C2S_proxy.h"
#include "../Common/S2C2S_stub.h"

// 클라이언트 캐릭터 종류 
enum	MyCharacter
{
	NONE = 0,
	C_FastFoodGuy,  // 1
	C_Biker,        // 2
	C_FireFighter,  // 3
	C_GamerGirl,    // 4
	C_Gangster,     // 5
	C_Grandma,      // 6
	C_Grandpa,      // 7
	C_HipsterGirl,  // 8
	C_HipsterGuy,   // 9
	C_Hobo,         // 10
	C_Jock,         // 11
	C_Paramedic,    // 12
	C_PunkGirl,     // 13
	C_PunkGuy,      // 14
	C_RoadWorker,   // 15
	C_ShopKeeper,   // 16
	C_SummerGirl,   // 17
	C_Tourist      // 18
};

// 게임 방 정보
class RoomInfo
{
public:
	int m_player_num; // 방 인원 수
	bool m_ready_game; // 게임이 시작 준비 여부
	bool m_end_game; // 게임이 끝난 여부

	int m_S_Team_num; // 사파이어 팀의 인원 수
	int m_R_Team_num; // 루비 팀의 인원 수

	wstring m_weather; // 방 날씨 Morning - 습도 감소, Evening - 무 효과, Dawn - 습도 상승.

	int m_In_game_num; // 카운트가 끝나고 방에 입장한 인원 수 
	bool m_Ontimer; // 타이머 시작 여부
	int m_Min, m_Sec; // 타이머 시간

	int m_S_Team_Kill; // 사파이어 킬 수
	int m_R_Team_Kill; // 루비 킬 수

	float m_items_x[5]; // 아이템 정보
	float m_items_y[5];
	float m_items_z[5];

};

// 클라이언트 클래스
class RemoteClient
{
public:
	wstring m_userID; // 유저의 HostID 값 (*KEY)
	int m_groupID;// 유저가 속한 P2P Group ID 값
	bool m_Response; // 유저 무적 여부
	
	wstring m_Team; // 유저의 팀 정보
	int m_TeamNum; // 유저의 팀 번호 ( * 리스폰 위치와 대기실 순서 )
	
	MyCharacter m_character; // 유저의 캐릭터 종류 정보
	int m_humidity; // 유저의 습도 ( * 체력 )

	float m_R_posX, m_R_posY, m_R_posZ; // 유저의 리스폰 위치 값
	float m_R_rotX, m_R_rotY, m_R_rotZ; // 유저의 리스폰 방향 값
	float m_posX, m_posY, m_posZ; // 유저의 현재 위치 값
	float m_rotX, m_rotY, m_rotZ; // 유저의 현재 방향 값
	float m_move; // 유저의 위치 입력 값
	float m_rotate; // 유저의 방향 입력 값
	bool reload; // 유저의 장전 입력 값
};

// 서버 클래스
class Aquarium_server
	:public S2C2S::Stub
{
public:

	// RMI 보내기 위한 모듈
	S2C2S::Proxy m_proxy;

	// 클라이언트의 연결을 받기위한 객체
	shared_ptr<CNetServer> m_server;

	// 멀티스레드의 경쟁 상태를 예방하기 위해서 잠금
	CriticalSection m_critSec;

	// 클라이언트의 정보
	// key : client HostID
	unordered_map<int, shared_ptr<RemoteClient> > m_Client_Infos;

	// P2P 방의 정보
	// key : P2PGroup HostID
	unordered_map<int, shared_ptr<RoomInfo> > m_Group_Infos;

	// P2P 그룹의 클라이언트 정보
	// key : P2PGroup HostID
	unordered_map<int, HostIDArray> m_playerGroups;

	// 방의 최대 인원 수
	int max_player_num = 4;
	
	// 날씨
	wstring weathers[3] = {L"Morning", L"Morning", L"Morning"};

	// 아이템들
	float m_Total_item_x[10] = { -92, -126, -100, -30, -50, -50, 14.5, 52.5, 52.5, 52.5 };
	float m_Total_item_y[10] = { 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4, 0.4 };
	float m_Total_item_z[10] = { 35, -4, -42, 40, -3.5, -42, -4, 80.5, 19.5, -42 };

	Aquarium_server()
	{
	}

	~Aquarium_server()
	{
	}

	void Run(); // 서버 런타임 함수

private:

	// 로그인 원격 함수
	DECRMI_S2C2S_RequestLogin;

	// 게임 방 검색 원격 함수
	DECRMI_S2C2S_Get_selete;

	// 게임 방 입장 원격 함수
	DECRMI_S2C2S_JoinGameRoom;

	// 진행 중인 게임 입장 원격 함수
	DECRMI_S2C2S_JoinInGame;

	// 게임 방 퇴장 원격 함수
	DECRMI_S2C2S_LeaveGameRoom;

	// 플레이어 이동 동기화
	DECRMI_S2C2S_Player_Move;

	// 플레이어 체력에 데미지 적용
	DECRMI_S2C2S_Player_SetHP;

	// 인 게임 접속이 됐을 때 호출.
	DECRMI_S2C2S_Player_SetReady;

	// 플레이어 무적 여부 원격 함수
	DECRMI_S2C2S_Player_SetResponse;

	// 게임 종료 원격 함수
	DECRMI_S2C2S_Get_END;

};